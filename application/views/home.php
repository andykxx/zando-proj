<div class="container">
	<div class="row">
		<h1>Welcome to Zando Products</h1>
	</div>
	<div class="row">
		<?php
			foreach ($products as $key) {
				# code...
				if($key['status'] == 'active' && $key['quality_approved'] == 1 && $key['price'] !== 'NULL' && $key['image_url'] !== '' && $key['brand'] !== ''){
					echo '<div class="col-md-3 card">';
					echo '<img src="'.$key['image_url'].'" alt="Card image cap">';
					echo '<span id="left">' .$key['name'].'</span>';
					echo '<span id="right">R ' .$key['price'].'</span>';
					echo '<p>' .$key['brand'].'</p>';
					echo '</div>';
				}
			}
		?>
	</div>
</div>