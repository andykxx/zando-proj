<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products_model extends CI_Model
{
    function __construct(){
        parent::__construct();
    }
    
    //get all products
    public function check_products($n){
        $sql = "SELECT * FROM `products` WHERE name= '$n'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function insert_products($name, $sku, $price, $status, $quality_approved, $image_url, $brand){
    	$data = array(
            'name'    			=> $name,
            'sku'        		=> $sku,
            'price'          	=> $price,
            'status'       		=> $status,
            'quality_approved'  => $quality_approved,
            'image_url'       	=> $image_url,
            'brand'         	=> $brand
        );
        if($this->db->insert('products', $data))
            return true;
        else
            return false;
    }
}