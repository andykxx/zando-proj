<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	//constructor
	public function __construct()
	{
        parent::__construct();
        $this->load->model('Products_model');
    }
	public function index()
	{
		$data['title'] 			= 'Home';
		$data['main_content'] 	= 'home';
		$data['products'] 		= $this->get_products_from_csv();
		$this->store_products_from_csv();
        $this->load->view('includes/template', $data);
	}

	function get_products_from_csv(){
		$p 			= FCPATH.'files/products.csv';
		$file 		= fopen($p, "r");
		//skip first line
        fgetcsv($file);
		$ntArr 		= array();
		while(($line = fgetcsv($file, 20000, ";")) !== FALSE){
			$ntArr[] = array(
						'name' 				=> $line[0],
						'sku' 				=> $line[1],
						'price' 			=> $line[2],
						'status'			=> $line[3],
						'quality_approved'	=> $line[4],
						'image_url'			=> $line[5],
						'brand'				=> $line[6]
					); 
		}
		return $ntArr;
	}
	
	//add the csv entries to the db, check for duplicates 
	function store_products_from_csv(){
		$p 			= FCPATH.'files/products.csv';
		$file 		= fopen($p, "r");
		//skip first line
        fgetcsv($file);
        while( ($line = fgetcsv($file, 20000, ";") ) !== FALSE ){
        	//check
        	$checkPrev = $this->Products_model->check_products($line[0]);
        	if(count($checkPrev) > 0){
        		//update?
        	}else{
        		//insert
        		$this->Products_model->insert_products($line[0], $line[1], $line[2], $line[3], $line[4], $line[5], $line[6]);
        	}
        }
	}
}
