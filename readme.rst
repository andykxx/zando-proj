###################
Zando Project
###################

The project uses Codeigniter 3.1.5 framework, jQuery and Bootstrap.

The application displays the products from the csv file, and also stores the entries on the database 
provided there are no duplicates.


*******************
Release Information
*******************


*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************

Import the products.sql database and edit the /application/config/database credentials to the one you use.


