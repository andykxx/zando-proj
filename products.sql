-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Sep 14, 2017 at 06:23 AM
-- Server version: 5.6.35
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zando`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `sku` varchar(15) NOT NULL,
  `price` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `quality_approved` int(2) NOT NULL,
  `image_url` varchar(50) NOT NULL,
  `brand` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `sku`, `price`, `status`, `quality_approved`, `image_url`, `brand`) VALUES
(1, 'Product 1', 'PRODUCT123', '499,25', 'active', 1, 'https://dummyimage.com/300x400/000/fff.jpg&text=PR', 'Zando'),
(2, 'Product 2', '', '493,25', 'active', 1, 'https://dummyimage.com/300x400/000/fff.jpg&text=PR', 'adidas'),
(3, 'Product 3', 'PRODUCT345', '1099,25', 'active', 1, 'https://dummyimage.com/300x400/000/fff.jpg&text=PR', 'adidas'),
(4, 'Product 4', 'PRODUCT456', 'NULL', 'active', 1, 'https://dummyimage.com/300x400/000/fff.jpg&text=PR', 'Zando'),
(5, 'Product 5', 'PRODUCT567', '100,99', 'inactive', 1, 'https://dummyimage.com/300x400/000/fff.jpg&text=PR', 'Zando'),
(6, 'Product 6', 'PRODUCT678', '100,99', 'active', 0, 'https://dummyimage.com/300x400/000/fff.jpg&text=PR', 'Zando'),
(7, 'Product 7', 'PRODUCT789', '99', 'active', 1, 'https://dummyimage.com/300x400/000/fff.jpg&text=PR', 'Zando'),
(8, 'Product 8', 'PRODUCT8910', '99', 'deleted', 1, 'https://dummyimage.com/300x400/000/fff.jpg&text=PR', 'Zando'),
(9, 'Product 9', 'PRODUCT91011', '123,45', 'active', 1, '', 'adidas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
